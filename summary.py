import math

from tabulate import tabulate

from covid.db import session
from covid.utils import abbreviate

QUERY = """
WITH by_date as (SELECT state,
                        (timestamp at time zone 'asia/almaty')::date as add_date,
                        sum(infected)                                as sum_infected,
                        sum(cured)                                   as sum_cured,
                        sum(dead)                                    as sum_dead
                 FROM infected
                 GROUP BY state, add_date),
     all_dates as (SELECT t.day::date
                   FROM generate_series((SELECT min(by_date.add_date) FROM by_date),
                                        (SELECT max(by_date.add_date) FROM by_date), '1 day'::interval) as t(day)),
     states as (SELECT DISTINCT by_date.state FROM by_date),
     dates_states as (SELECT *
                      FROM all_dates
                               CROSS JOIN states),
     full_table as (SELECT ds.day,
                           ds.state,
                           coalesce(bd.sum_infected, 0) as sum_infected,
                           coalesce(bd.sum_cured, 0)    as sum_cured,
                           coalesce(bd.sum_dead, 0)     as sum_dead
                    FROM dates_states ds
                             LEFT JOIN by_date bd ON ds.day = bd.add_date AND ds.state = bd.state
                    ORDER BY state, ds.day),
     cum_table as (SELECT ft.day,
                          ft.state,
                          sum(ft.sum_infected) OVER (PARTITION BY ft.state ORDER BY ft.day) cum_infected,
                          sum(ft.sum_cured) OVER (PARTITION BY ft.state ORDER BY ft.day)    cum_cured,
                          sum(ft.sum_dead) OVER (PARTITION BY ft.state ORDER BY ft.day)     cum_dead
                   FROM full_table ft),
     lag_table as (SELECT ct.day,
                          ct.state,
                          ct.cum_infected,
                          LAG(ct.cum_infected, 1) OVER (PARTITION BY ct.state ORDER BY ct.day) cum_infected_1,
                          LAG(ct.cum_infected, 3) OVER (PARTITION BY ct.state ORDER BY ct.day) cum_infected_3,
                          ct.cum_cured,
                          LAG(ct.cum_cured, 1) OVER (PARTITION BY ct.state ORDER BY ct.day)    cum_cured_1,
                          LAG(ct.cum_cured, 3) OVER (PARTITION BY ct.state ORDER BY ct.day)    cum_cured_3,
                          ct.cum_dead,
                          LAG(ct.cum_dead, 1) OVER (PARTITION BY ct.state ORDER BY ct.day)     cum_dead_1,
                          LAG(ct.cum_dead, 3) OVER (PARTITION BY ct.state ORDER BY ct.day)     cum_dead_3
                   FROM cum_table ct),
     last_lag as (SELECT *
                  FROM lag_table
                  WHERE day = (SELECT MAX(day) FROM lag_table)),
growth as (SELECT ll.day,
       ll.state,
       CASE WHEN ll.cum_infected_3 > 0 THEN pow(ll.cum_infected / ll.cum_infected_3, 1.0/3) END infected3,
       CASE WHEN ll.cum_infected_1 > 0 THEN ll.cum_infected / ll.cum_infected_1 END infected1,
       CASE WHEN ll.cum_cured_3 > 0 THEN pow(ll.cum_cured / ll.cum_cured_3, 1.0/3) END cured3,
       CASE WHEN ll.cum_cured_1 > 0 THEN ll.cum_cured / ll.cum_cured_1 END cured1,
       CASE WHEN ll.cum_dead_3 > 0 THEN pow(ll.cum_dead / ll.cum_dead_3, 1.0/3) END dead3,
       CASE WHEN ll.cum_dead_1 > 0 THEN ll.cum_dead / ll.cum_dead_1 END dead1
FROM last_lag ll)
SELECT state, (infected1 - 1) * 100 as growth1, (infected3 - 1) * 100 as growth3, CASE WHEN infected3 > 1 THEN 1 / log(2, infected3) END as double_index
FROM growth
ORDER BY growth3
"""


def get_data():
    data = session.execute(QUERY).fetchall()
    return data


def format_report(data):
    result = []
    for state, one_day, three_days, di in data:
        one_day = "-" if one_day is None else 0 if math.isclose(one_day, 0) else "{:.0f}".format(one_day)
        three_days = "-" if three_days is None else 0 if math.isclose(three_days, 0) else "{:.0f}".format(three_days)
        di = '-' if di is None else "{:.0f}".format(di)
        state = abbreviate(state)
        result.append((state, three_days, one_day, di))
    return [
        "Динамика распространения COVID-19",
        "П3 - средний суточный прирост за последние 3 дня, %",
        "П1 - прирост за последний день, %",
        "x2 - число дней до удвоения при приросте П3",
        "",
        tabulate(result, ("Регион", "П3", "П1", "x2"))
    ]


def dynamics_report():
    return format_report(get_data())

def main():
    print()

if __name__ == '__main__':
    main()