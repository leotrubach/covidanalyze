import os
import pathlib
import dotenv
import pytz

ROOT_DIR = pathlib.Path(__file__).parent

dotenv.load_dotenv()


TEST_RUN = bool(int(os.getenv("TEST_RUN", "1")))
NOMINATIM_DOMAIN = "127.0.0.1:7070"
GOOGLE_CREDENTIALS = ROOT_DIR / "credentials.json"
timezone = pytz.timezone("Asia/Almaty")
TELEGRAM_BOT_TOKEN = os.getenv("TELEGRAM_BOT_TOKEN")
REAL_CHAT_ID = os.getenv("TEST_CHAT_ID")
TEST_CHAT_ID = os.getenv("REAL_CHAT_ID")

if TEST_RUN:
    CHAT_ID = TEST_CHAT_ID
else:
    CHAT_ID = REAL_CHAT_ID
SPREADSHEET_NAME = os.getenv("SPREADSHEET_NAME")