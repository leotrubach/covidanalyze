import telegram.ext
from telegram.ext import Updater

from covid.report.covidmap import full_report
from covid.report.summary import map_report
from covid.utils import wrap_monospace
from settings import TELEGRAM_BOT_TOKEN, CHAT_ID
from summary import dynamics_report


u = Updater(TELEGRAM_BOT_TOKEN, use_context=True)
j = u.job_queue


def callback_hourly(context: telegram.ext.CallbackContext):
    messages_to_send = [
        full_report(),
        map_report(),
        dynamics_report()
    ]
    for lines in messages_to_send:
        context.bot.send_message(
            chat_id=CHAT_ID,
            text='\n'.join(wrap_monospace(lines)),
            parse_mode=telegram.ParseMode.MARKDOWN,
        )


job_minute = j.run_repeating(callback_hourly, interval=60 * 60, first=0)
j.start()
