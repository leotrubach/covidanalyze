import requests
from geopy.geocoders import Nominatim
from sqlalchemy import func

from covid.db import session, Patient
from settings import NOMINATIM_DOMAIN

geolocator = Nominatim(scheme="http", domain=NOMINATIM_DOMAIN)

URL = "https://m.egov.kz/covid-proxy-app/api/v1/covid/patient"
HEADERS = {
    "Accept": "application/json, text/javascript, */*; q=0.01",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7,kk;q=0.6",
    "Connection": "keep-alive",
    "Cookie": "egov-client-type=PORTAL; _ga=GA1.2.309550876.1584921609; _ym_uid=1584921610557654268; _ym_d=1584921610; has_js=1; _offer_app=1; egovLang=ru; SSO=8db2ee28-bedb-4c32-ba6d-c8712a68b2c0; OPENDATA_PORTAL_FLASH=url=%2Fdatasets%2Fsearch%3Ftext%3Dcovid%26ok%3D%25D0%2598%25D1%2581%25D0%25BA%25D0%25B0%25D1%2582%25D1%258C%26expType%3D0%26govAgencyId%3D%26category%3D%26pDateBeg%3D%26pDateEnd%3D%26statusType%3D%26actualType%3D%26datasetSortSelect%3DcreatedDateDesc; SSESSf0d09e6725a5b1994b1b0ab11a8d594d=Hu3GD9zIfsSOVjBsKLDoP5JgBrSMeUc6ybSm6EOR4Q0; _ym_isad=1; _gid=GA1.2.1665526498.1585494052; _ym_visorc_13838020=b; _gat_gtag_UA_30759057_1=1",
    "Host": "m.egov.kz",
    "Referer": "https://m.egov.kz/covid/app/map.html",
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-origin",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36",
    "X-Requested-With": "XMLHttpRequest",
}


def get_location(lat, lon):
    return geolocator.reverse((lat, lon))


def get_data():
    return requests.get(URL, headers=HEADERS).json()


def parse_coords(value):
    try:
        return float(value)
    except ValueError:
        parts = value.split(".")
        fixed = ".".join([parts[0], "".join(parts[1:])])
        return float(fixed)


def fetch():
    data = get_data()
    if not data:
        return [], []
    patients = list(session.query(Patient).all())
    data_ids = set(o["id"] for o in data)
    db_ids = set(p.id for p in patients)
    to_add = list(filter(lambda o: o["id"] not in db_ids, data))
    active_ids = set(p.id for p in patients if p.end is None)
    added = []
    to_exclude = list(active_ids - data_ids)
    if not to_add and not to_exclude:
        return [], []
    for o in to_add:
        try:
            lat = parse_coords(o["latitude"])
            lon = parse_coords(o["longtitude"])
        except ValueError:
            continue
        added.append(o)
        location = get_location(lat, lon)
        address = location.raw.get("address")
        if not address:
            city = "?"
            county = "?"
            state = "?"
            address_txt = "?"
        else:
            city = address.get("city")
            county = address.get("county")
            state = address.get("state")
            address_txt = location.address
        if state is None and city == "Алматы":
            state = city
        p = Patient(
            id=o["id"],
            latitude=lat,
            longitude=lon,
            status=o["status"],
            city=city or county,
            address=address_txt,
            state=state,
        )
        o["city"] = city or county
        o["address"] = address_txt
        o["state"] = state
        session.add(p)
    if to_exclude:
        session.query(Patient).filter(Patient.id.in_(to_exclude)).update(
            {Patient.end: func.now()}, synchronize_session=False
        )
    session.commit()
    return added, to_exclude

