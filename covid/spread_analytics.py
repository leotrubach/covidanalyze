import datetime
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from covid.db import session, Details

from settings import GOOGLE_CREDENTIALS, timezone, SPREADSHEET_NAME

scope = [
    "https://spreadsheets.google.com/feeds",
    "https://www.googleapis.com/auth/drive",
]

credentials = ServiceAccountCredentials.from_json_keyfile_name(
    GOOGLE_CREDENTIALS, scope
)

gc = gspread.authorize(credentials)

book = gc.open(SPREADSHEET_NAME)
wks = book.sheet1
data = wks.get_all_values()
headers = data.pop(0)

today = datetime.datetime.now(tz=timezone)


def parse_int(x):
    if x in ('0', '00'):
        return 0
    else:
        return int(x.lstrip("0"))


def parse_int2(x):
    v = x.strip()
    if not v:
        return 0
    else:
        return int(x)


def fetch_spread():
    result = []
    for date, time, region, infected, cured, dead, _ in data:
        if not date:
            break
        day, month = map(parse_int, date.split("/"))
        hour, minute = map(parse_int, time.split(":"))
        dt = datetime.datetime(today.year, month, day, hour, minute, tzinfo=timezone)
        result.append([dt, region, parse_int2(infected), parse_int2(cured), parse_int2(dead)])
    return result


def main():
    data = fetch_spread()
    for dt, region, infected, cured, dead in data:
        d = Details(state=region, timestamp=dt, infected=infected, cured=cured, dead=dead)
        session.add(d)
    session.commit()


if __name__ == '__main__':
    main()