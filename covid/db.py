from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float, DateTime, func
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


Base = declarative_base()
engine = create_engine(
    "postgresql://postgres:123456789@127.0.0.1:5441/postgres", echo=True
)
Session = sessionmaker(bind=engine)
session = Session()


class Patient(Base):
    __tablename__ = "patients"

    id = Column(Integer, primary_key=True)
    status = Column(Integer)
    longitude = Column(Float)
    latitude = Column(Float)
    city = Column(String)
    state = Column(String)
    address = Column(String)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())
    end = Column(DateTime(timezone=True))

    def __repr__(self):
        return f"<Patient id={self.id} status={self.status} ({self.latitude}, {self.longitude})"


class Stats(Base):
    __tablename__ = "stats"
    city = Column(String, primary_key=True)
    infected = Column(Integer)
    cured = Column(Integer)
    dead = Column(Integer)


class Details(Base):
    __tablename__ = "infected"
    id = Column(Integer, primary_key=True)
    state = Column(String)
    timestamp = Column(DateTime(timezone=True))
    infected = Column(Integer)
    cured = Column(Integer)
    dead = Column(Integer)

Base.metadata.create_all(engine)
