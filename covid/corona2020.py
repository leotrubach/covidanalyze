from collections import defaultdict

import requests
from html5_parser import parse

from covid.db import session, Stats


HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-US,en;q=0.9,ru-RU;q=0.8,ru;q=0.7,kk;q=0.6",
    "Cache-Control": "max-age=0",
    "Connection": "keep-alive",
    "Cookie": "_ga=GA1.2.1320219886.1584954716; _gid=GA1.2.259804588.1585518807",
    "Host": "www.coronavirus2020.kz",
    "Sec-Fetch-Dest": "document",
    "Sec-Fetch-Mode": "navigate",
    "Sec-Fetch-Site": "none",
    "Sec-Fetch-User": "?1",
    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36",
}

URL = "https://www.coronavirus2020.kz/"

XPATHS = {
    "infected": "//div[@class='last_info_covid_bl']//div[@class='city_cov']/div",
    "cured": "//div[@class='red_line_covid_bl']//div[@class='city_cov']/div",
    "dead": "//div[@class='deaths_bl']//div[@class='city_cov']/div",
}


def process(t):
    region_str, amount_str = map(str.strip, t.split("–"))
    region = region_str.replace("г.", "").strip()
    amount = int(amount_str)
    return region, amount


def process_elements(elements):
    return list(map(process, (e.text for e in elements)))


def page_parse():
    resp = requests.get(URL, headers=HEADERS)
    tree = parse(resp.text)
    parsed = {
        kind: process_elements(tree.xpath(xpath)) for kind, xpath in XPATHS.items()
    }
    transformed = defaultdict(dict)
    for kind, city_list in parsed.items():
        for city_name, amount in city_list:
            transformed[city_name][kind] = amount
    result = []
    for city_name, stats in transformed.items():
        result.append(
            (
                city_name,
                stats.get("infected", 0),
                stats.get("cured", 0),
                stats.get("dead", 0),
            )
        )
    return result


def main():
    last_stats = page_parse()
    session.query(Stats).delete(synchronize_session=False)
    for city, infected, cured, dead in last_stats:
        s = Stats(city=city, infected=infected, cured=cured, dead=dead)
        session.add(s)
    session.commit()


if __name__ == "__main__":
    main()
