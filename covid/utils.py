ABBR_PAIRS = (
    ("Восточно-Казахстанская область", "ВКО"),
    ("Северо-Казахстанская область", "СКО"),
    ("Западно-Казахстанская область", "ЗКО"),
    ("область", ""),
)


def abbreviate(word):
    w = word
    for what, to in ABBR_PAIRS:
        w = w.replace(what, to)
    return w


def wrap_monospace(lines):
    return [
        "```",
        *lines,
        "```"
    ]