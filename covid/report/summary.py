import tabulate
from sqlalchemy import func

from covid.db import session, Patient, Stats
from covid.utils import abbreviate


def map_report():
    cte = (
        session.query(Patient.state, func.count(Patient.id))
            .filter(Patient.end.is_(None))
            .group_by(Patient.state)
            .order_by(func.count(Patient.id).desc())
            .cte()
    )
    mq = session.query(cte, Stats).join(
        Stats, cte.c.state == Stats.city, isouter=True
    )
    data = []
    for o in mq:
        if hasattr(o, "Stats") and o.Stats is not None:
            data.append((o[0], o[1], o.Stats.infected, o.Stats.cured, o.Stats.dead))
        else:
            data.append((o[0], o[1], 0, 0, 0))
    totals = list(map(sum, zip(*[o[1:] for o in data])))
    total_contacts, total_infected, total_cured, total_dead = totals
    tabulated = tabulate.tabulate(
        [
            [
                abbreviate(reg),
                "{}/{}".format(amount, infected),
                "{}/{}".format(cured, dead),
            ]
            for reg, amount, infected, cured, dead in data
        ],
        headers=["Регион", "K/И", "В/Л"],
    )
    summary_report = [
        f"К - контактировавшие ({total_contacts})",
        f"И - инфицированные ({total_infected})",
        f"В - выздоровевшие ({total_cured})",
        f"Л - летальные случаи ({total_dead})",
        tabulated,
    ]
    return summary_report