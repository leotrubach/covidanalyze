import datetime
from itertools import groupby
from typing import Tuple

import pytz
from sqlalchemy import func, tuple_, table, column, select, and_

from covid.db import session


tz = pytz.timezone("Asia/Almaty")
oneday = datetime.timedelta(days=1)
t = table(
    "patients",
    column("id"),
    column("state"),
    column("city"),
    column("timestamp"),
    column("end"),
)


def get_dates():
    today = (
        datetime.datetime.now()
        .astimezone(tz)
        .replace(hour=0, minute=0, second=0, microsecond=0)
    )
    yesterday = today - oneday
    return yesterday, today


def get_totals(period: Tuple[datetime.datetime, datetime.datetime], added=True):
    start, end = period
    stmt = select([t.c.state, t.c.city, func.count(t.c.id).label("count")])
    if added:
        stmt = stmt.where(and_(t.c.timestamp >= start, t.c.timestamp < end))
    else:
        stmt = stmt.where(and_(t.c.end >= start, t.c.end < end))
    stmt = stmt.group_by(
        func.grouping_sets(tuple_(), tuple_(t.c.state), tuple_(t.c.state, t.c.city))
    ).order_by(t.c.state, t.c.city.nullsfirst())
    return session.execute(stmt).fetchall()


def report(data):
    result = []
    for state, rows in groupby(data, lambda x: x[0]):
        for state, city, count in rows:
            if state is None:
                result.append("-----------------")
                result.append(f"Всего: {count}")
            elif city is None:
                result.append(f"{state} ({count}):")
            else:
                result.append(f"  {city} - {count}")
    return result


def full_report():
    yesterday, today = get_dates()
    added = get_totals((yesterday, today), True)
    removed = get_totals((yesterday, today), False)
    return [
        f"Сводка за {yesterday.date()}\n"
        "Зарегистрированно новых контактировавших:",
        *report(added),
        "",
        "Исключено из списка:",
        *report(removed)
    ]
